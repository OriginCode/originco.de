+++
title = "音樂遊戲角"
template = "section.html"
+++

## 音樂遊戲角

我目前遊玩的音遊主要有：beatmania IIDX, maimai でらっくす, osu!mania (4k), Etterna, BMS (Beatoraja, Controller)

不常玩但是會玩的/正在嘗試的音遊有：SOUND VOLTEX, Dance Dance Revolution, WACCA, CHUNITHM, オンゲキ, Jubeat

因為各種原因目前沒有在玩移動端音遊，曾經主要遊玩過 Arcaea（我的音遊入坑作）

## 音遊組曲列表

- [beatmania IIDX 2024 課題 渚](/mug/iidx)
- [beatmania IIDX 2023 課題 翼](/mug/iidx-2023)
- [beatmania IIDX 2022 課題 鴉](/mug/iidx-2022)
- [X CROSS](/mug/x-cross)
- [V CROSS](/mug/v-cross)
