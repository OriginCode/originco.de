+++
title = "V CROSS"
date = 2023-01-25
+++

Feryquitous "V" シリーズ

| Title    | Year | Game              | Difficulty                                  |
|----------|------|-------------------|---------------------------------------------|
| Dstorv   | 2014 | Be Music Source   | 7KEYS: ☆4 (▽3), ☆7 (▽7), ☆10 (▽10)          |
| Strahv   | 2015 | Be Music Source   | 7KEYS: ☆8, ☆10, ☆12 (★9)                    |
| Rhuzerv  | 2018 | Deemo             | Easy LV3, Normal LV7, Hard LV11             |
| Visterhv | 2019 | beatmania IIDX    | SP: N6, H10, A12; DP: N6, H10, A12          |
| Arcahv   | 2020 | Arcaea            | PST4 (4.5), PRS7 (7.5), FTR9+ (9.9)         |
| Rhuzerv  | 2021 | Cytus II          | EASY 5, HARD 8, CHAOS 15                    |
| Estahv   | 2022 | maimai でらっくす | BASIC 4, ADVANCED 7, EXPERT 11+, MASTER 13+ |
