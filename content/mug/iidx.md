+++
title = "beatmania IIDX 課題組曲 (2024 - 渚)"
date = 2023-12-14
+++

beatmania IIDX 課題組曲，娛樂用

## 食用方法

使用 EASY 以上血條依次通關組曲內四首曲目即可。

挑戰：使用 HARD 以上血條依次通關組曲內四首曲目即可。

需在 beatmania IIDX 30 及以上的版本遊玩。

## 渚初階（推薦段位：六段）

| Title           | Difficulty |
|-----------------|------------|
| 儚き恋の華      | SPH9       |
| Kailua          | SPH9       |
| 雪上断火        | SPH10      |
| BLACK or WHITE? | SPH10      |

## 渚二階（推薦段位：七段）

| Title                    | Difficulty |
|--------------------------|------------|
| AA                       | SPH10      |
| RAGE feat.H14 of LEONAIR | SPH10      |
| Donkey Donk              | SPA10      |
| DEEP ROAR                | SPA11      |

## 渚三階（推薦段位：八段）

| Title                           | Difficulty |
|---------------------------------|------------|
| Banger Banger Banger Banger     | SPA11      |
| 月とミルク                      | SPA11      |
| Chrono Diver -PENDULUMs-        | SPH11      |
| Bahram Attack-猫叉Master Remix- | SPL11      |

## 渚四階（推薦段位：九段）

| Title       | Difficulty |
|-------------|------------|
| Little Star | SPA12      |
| Caterpillar | SPA12      |
| 刃図羅      | SPA12      |
| TIEFSEE     | SPA12      |

## 渚五階（推薦段位：十段）

| Title         | Difficulty |
|---------------|------------|
| Lethal Weapon | SPA12      |
| Adularia      | SPA12      |
| Super Rush    | SPA12      |
| Arkadia       | SPA12      |
