+++
title = "beatmania IIDX 課題組曲 (2022 - 鴉)"
date = 2022-09-01
+++

beatmania IIDX 課題組曲，娛樂用

## 歷史更改

- 2022-09-04 初步確定鴉初階～鴉四階，細分段位
- 2022-09-01 擬定鴉三階，初步制定鴉初階

## 食用方法

使用 EASY 血條依次通關組曲內四首曲目即可。

需在 beatmania IIDX 28 及以上的版本遊玩。

## 鴉初階

| Title                   | Difficulty    |
|-------------------------|---------------|
| Happy Wedding           | SPA8          |
| SAMURAI-Scramble        | SPH9          |
| SA.YO.NA.RA. SUPER STAR | SPH9          |
| BEAT PRISONER           | SPH9          |

## 鴉二階

| Title          | Difficulty    |
|----------------|---------------|
| AO-∞           | SPH9          |
| Watch Out Pt.2 | SPH9          |
| EROICA         | SPH10         |
| DOMINION       | SPH10         |

## 鴉三階

| Title                            | Difficulty    |
|----------------------------------|---------------|
| Ɐ                                | SPH10         |
| 灼熱Beach Side Bunny             | SPH10         |
| ラストセンチュリーメランコリック | SPA10         |
| BLUE DRAGON(雷龍RemixIIDX)       | SPH10         |

## 鴉四階

| Title               | Difficulty |
|---------------------|------------|
| Elisha              | SPA11      |
| 仮想空間の旅人たち  | SPA11      |
| Red. by Jack Trance | SPH10      |
| Evans               | SPH11      |
