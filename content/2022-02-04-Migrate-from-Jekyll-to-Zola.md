+++
title = "博客從 Jekyll 遷移到 Zola"
date = 2022-02-04

[taxonomies]
tags = ["Blog"]
+++

今天晚上閒來無事 ~~（並不，還有一個 Essay 週末 Due）~~ ，將博客從 Jekyll 遷移到了 Zola 上。

遷移過程還算輕鬆，首先是去 [Zola 官網上](https://www.getzola.org/themes/)找了找我覺得不錯的主題，然後開始暴改（並沒有）。

<!-- more -->

最後選定的主題是（好像）官方出的 After Dark。外觀感覺很棒，唯一缺點就是有點太簡陋了，連 About 都沒有，更別說 Links 了（

然後就開始改動主題，基本沒看官方文檔（別學我）。從各個主題那邊看了看抄了抄，最後確定下來如你所見的模樣。

這次更換靜態網頁生成器，一方面是覺得 GitHub Pages + Jekyll 的東西跑在 GitLab Pages 上實在有點 cursed，另一方面是 Zola 有生鏽加持（x

總之以後也請多多關照。
