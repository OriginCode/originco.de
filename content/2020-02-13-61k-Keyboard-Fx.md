+++
title = "61鍵（60%）鍵盤在 Arch Linux 上的 F1-F12 鍵修復"
date = 2020-02-13

[taxonomies]
tags = ["Arch", "Linux"]
categories = ["Linux"]
+++

> 說實話小鍵盤挺好用的

前段陣子入手了一塊 RK61 鍵盤，一方面是用於替換之前被肥宅快樂水泡過的筆記本自帶鍵盤（被泡過後導致按鍵反應十分遲鈍，可樂真的黏），另一方面則是爲了玩音遊 ~~（這才是主要原因）~~ 。在疫情之下等了幾天，到手後用上那叫一個爽快。我買的是青軸，十分清脆 ~~（只要不擔心被別人打死）~~ ，比肥宅快樂水筆記本鍵盤不知道好到哪去了。

<!-- more -->

由於是 61 鍵的配置，有些按鍵需要使用 Fn+Key 的組合鍵來達成，其中就包括了 F1-F12 的功能鍵，官方說明了是使用 Fn+1-12-+ 組合鍵，即可使用 F1-F12。

然而當我在 Arch Linux 上面的時候，問題來了。使用時，會發現 Fn+1-12-+ 會調用一些奇怪的功能比如增減音量、熒幕亮度，甚至播放音樂。一開始我以爲這是 Arch Linux 或者我使用的 KDE 的一些奇怪的映射導致的。然而我連 Ctrl+Alt+Fn+1-12-+ 來切換 TTY 的組合鍵都無法使用，且當我使用我電腦上其他的 Linux (Gentoo Linux, AOSC OS) 和 Windoge 的時候沒有這個問題。毫無思緒，我只好把 Yakuake 的快捷鍵之類都換成不使用 F1-F12 鍵的，而切換 TTY 什麼的就又必須要用回可樂鍵盤。

今天下午在咕鴿上面搜了一下問題，發現似乎這種 61 鍵鍵盤在 Linux 上都有機率出現這個問題，最終在 [F1-F12 keys don't work](https://unix.stackexchange.com/questions/371243/f1-f12-keys-dont-work) 這篇問答下面看到了解決方案。

    change behaviour on the fly 
    # echo 2 > /sys/module/hid_apple/parameters/fnmode

這種辦法是一時的，需要長期保持 F1-F12 的話需要寫一個 `/etc/modprobe.d/hid_apple.conf` ，加入 `options hid_apple fnmode=2` 。

然而重啓後檢查 `/sys/module/hid_apple/parameters/fnmode` 卻發現依舊是默認的 `1`。於是使用 hid_apple 關鍵字在 Arch Wiki 上搜索，搜到了 [Function keys do not work](https://wiki.archlinux.org/index.php/Apple_Keyboard#Function_keys_do_not_work) 這個東西，說明需要在檔案更改完畢後重新 `mkinitcpio`（前提確保 `/etc/mkinitcpio.conf` `HOOKS` 變數中有 `modconf` 或者上面的檔案的路徑在 `FILES` 變數中）。一路 `mkinitcpio`，重啓，解決。

順帶一提還發現這玩意兒還是水果廠鍵盤的東西？怪不得是 hid_**apple**😂
