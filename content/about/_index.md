+++
title = "我是何儂？"
template = "section.html"
+++

## 千須末記事屋

**本站內容如無特殊標識，均使用 CC-BY-NC-SA 4.0 協議。**

Built with GitLab Pages, Zola + Rust.

接隻網站用嗰模板徠搿墶 [after-dark](https://github.com/getzola/after-dark)。

### 我是何儂？

又叫 COMPL.EXE, CONN.EXE, OriginCode (abbr. OC), XtenDimension, Force0x1。

B.S. in [Informatics](https://ischool.uw.edu/), Senior at
[University of Washington](https://www.washington.edu/)

[GitHub](https://github.com/OriginCode), [GitLab](https://gitlab.com/OriginCode)

官話-2 (普通话-N 國語-5 其他-2) / 吳語-2 (餘杭話-3 杭州話語-1 上海閒話-1) /
English-3 / Esperanto-1 / Français-1

粵語-0 / 閩語-0 / 客家話-0

[異性戀為主（Heteroflexibility）](https://en.wikipedia.org/wiki/Heteroflexibility)
、我用 He/Him/They/Them/伊/渠 人稱代詞。

[AOSC](https://aosc.io)成員，AOSC OS 使用者。

喜歡的編程語言：Rust, Racket, Haskell

Ingress <img src="/Enlightened_Logo.webp" alt="ENLIGHTENED LOGO" width="15px" height="25px"><span style="color: #02BF02">ENLIGHTENED</span> L15 @ NR13-ROMEO-08 (previously AS14-PAPA-05/06)

### 音遊
- beatmania IIDX (32 Pinky Crush SP 中伝 DP 七段)
- BMS (発三 sl5)
- GITADORA (GALAXY WAVE DM 紫)
- DanceDanceRevolution (A3 六段)
- Etterna (EO 20.xx) (不常玩)
- osu!mania (42xx pp) (不常玩)
- maimai でらっくす (FESTiVAL でらっくすRATING 154xx) (退坑)
- SOUND VOLTEX (6代 EXCEED GEAR 剛力羅) (退坑)
- pop'n music (UniLab 81.xx 仙人) (不常玩)
- EZ2ON REBOOT : R (8K 入門)

### 聯絡

Telegram: [@OriginCode](https://t.me/OriginCode)

IRC: OriginCode@irc.libera.chat

Matrix: [i@m.origincode.me](https://matrix.to/#/@i:origincode.me)

E-Mail: [self@origincode.me](mailto:self@origincode.me)

[`GPG Pubkey Fingerprint: 0x357F978888888888`](https://keys.openpgp.org/vks/v1/by-fingerprint/FA5F9338FDC65B25B16D1E7C357F978888888888)
