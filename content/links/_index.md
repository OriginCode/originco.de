+++
title = "連結"
template = "section.html"
+++

## 朋友連結

### bruceutut

***

> ~~今天兔兔被煲了嗎？~~

[Bruce Z Blog](http://blog.brucezhang.cf/)

### ヨイツの賢狼ホロ

***

> 萌狼
>
> 雖說是狼，但是似乎很容易親近的樣子
>
> AOSCC 2019 的時候面基啦！~~好小隻的萌狼~~

[約伊兹的萌狼鄉手札](https://blog.yoitsu.moe/)

### 新一 A-Wing

***

> Arch Linux CN 成員，很厲害的樣子w

[新一醬的博客](https://a-wing.top/)

### 李皓奇 Liolok

***

> ~~擼貓.gif~~，坂本先生！

[liolok.com](https://liolok.com/)

### Megumi_fox 惠醬

***

> Arch Linux CN 成員，有一隻~~一直在咕咕~~的 /me TG Bot。

[惠醬的博客](https://blog.megumifox.com/)

### AxionL 艾老師

***

> Arch Linux CN 成員之一，是隻文科超厲害的艾老師！

[初等記憶體](https://axionl.me/)

### Amane Tobiichi 皮醬

***

> 似乎因爲一些原因而離開了咱們的皮醬。(Sad)

[余憶留聲機](https://amane.live/)

### Nicholas Wang 尼古拉斯

***

> 似乎不常在 Arch CN 羣發言（？）

[Nicholas Wang's Blog](https://nicho1as.wang/)

### FiveYellowMice 黃鼠鼠鼠鼠鼠

***

> 黃鼠，面基過的超可愛的黃鼠！
>
> 總是有一些奇思妙想，思想很活躍的黃鼠鼠。

[FYM Blog](https://fiveyellowmice.com/)

### Farseerfc 老師

***

> Arch Linux TU，Arch Linux CN 元老。
>
> 在大阪做助教，是十六夜咲夜控（小咲夜出世啦！）

[Farseerfc的小窩](https://farseerfc.me/)

### ShadowRZ 雨宮戀葉

***

> 可愛的和咱歲數差不多的孩子。
>
> 似乎和家長有些思想隔閡，不過希望能夠解決呢。
>
> 因爲和家長的矛盾曾退出過 Arch Linux CN。（不知道這次能待多久呢）

[Chaotic Dimenstion](https://shadowrz.github.io/)

### Alynx Zhou

***

> 超電磁炮控~~Bilibili!~~
>
> 好像經常在B站直播，~~超帥的~~
>
> 其實很早就有關注，那時候咱還沒接觸 Arch Linux，是因爲~~咕咕咕的~~C語言教程而關注到的

[喵's StackHarbor](https://sh.alynx.moe/)

### Nick Cao

***

> 活宅！（親口說的 Description）

[Nick’s Nichijou](https://nichi.co/)

### Lilydjwg 依雲

***

> Arch Linux CN 核心成員之一。技術上超厲害的百合姐姐！
>
> ubx   這個用戶只用用戶框說話
>
> 狐狸w

[依雲's Blog](https://blog.lilydjwg.me/)

### Astrian Zheng

***

> 音遊大佬！Ingress 大佬！
>
> 有一個很厲害的 IFS RSVP Bot 呢！

[本格異想錄](https://astrianzheng.cn/)

### Mike Yuan

***

> sdl tql wsl
>
> 比咱小的超級大佬QAQ

[YHNdnzj's Blog](https://yhndnzj.com/)

### Leo Shen

***

> ~~咕了好久沒寫~~
>
> 好耶是 Ingress 玩家
>
> ~~這麼說Linux 用戶和 Ingress 玩家的重疊率有點高~~

[Leo's Field](https://szclsya.me/)

### 派茲

***

> 莓办法 银不了 尽梨了
>
> Kedama Minecraft Server Player

[派茲的小站](https://blog.blw.moe/)

***

### 神楽坂卡諾 & 神楽坂鵯凪

> 是可愛卡諾和可愛糰子=w=

[神楽坂の猫](https://kagurazaka.cat/)
