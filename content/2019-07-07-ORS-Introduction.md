+++
title = "OriginCode Remote Service"
date = 2019-07-07

[taxonomies]
tags = ["ORS", "Linux", "Arch"]
categories = ["Linux"]
+++

OriginCode Remote Service，簡稱 ORS

在本年，我開始濫用了我的 VPS，用於給大家提供一些便民（不）服務，下為我的 VPS 所提供的服務。

<!-- more -->

服務總站：[OriginCode Remote Service](https://origincode.me)

1. Arch Linux User Repository - [origincode]

```sh
[origincode]
Server = https://repo.origincode.me/repo/$arch
Server = https://build.archlinuxcn.org/~origincode/repo/$arch
```

GPG Sign Key:  `0A5BAD445D80C1CC` & `62BF97502AE10D22`

1. Android Captive Portal

```sh
adb shell "settings put global captive_portal_http_url http://captive.origincode.me/generate_204"
adb shell "settings put global captive_portal_https_url https://captive.origincode.me/generate_204"
```

~~3. Arch Linux CN TG <=> Discord Bridge （除主群外）， Discord Join Link: [Here](https://discord.gg/jAhkYX)~~ 由於某個人的原因，本條廢棄且 Discord 羣組不再活動。

1. [某默默無聞的 OpenWeatherMap 查詢 Bot](https://t.me/OC_OWBot)
