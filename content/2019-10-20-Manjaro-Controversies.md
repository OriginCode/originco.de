+++
title = "【譯】Manjaro 的爭議"
date = 2019-10-20

[taxonomies]
tags = ["Arch", "Linux", "Manjaro"]
categories = ["Linux"]
+++

### 介紹

[Manjaro](https://zh.wikipedia.org/wiki/Manjaro_Linux) 是一個基於 [Arch Linux](https://zh.wikipedia.org/wiki/Arch_Linux) 的一個 Linux 發行版。它注重於易用性、使用者友善和穩定性。

然而，Manjaro 數年來有幾個主要的爭議。該 Gist 意於指出所有圍繞 Manjaro 的已知爭議。

<!-- more -->

### 穩定性

> Manjaro [...] 易於使用且十分穩定

Manjaro 宣稱其爲穩定的 Linux 發行版，然而他們所做的僅僅是把 Arch 的軟體包保留在源中一個星期，爲了所謂的“測試”——以保證他們的包不會在使用者安裝過程中出現問題。

### 適合新手

> Manjaro 同時適用於新手和有經驗的 Linux 老手
>
> Manjaro 適用於新人 [...][^1]

Manjaro 是一個漸進式升級發行版，而漸進式升級發行版由於一些原因**不應**被新人所使用。

你需要確保你的系統總是最新，這需要至少每週一次的系統更新。

由於軟體包在準備完後即釋出，你需要準備好應對潛在的問題和漏洞。

使用一個漸進式發行版時，你需要熟悉如何解決問題，瞭解包管理器是如何工作的，瞭解包與包之間是如何關聯的，以及還要瞭解依賴關係是如何工作的。同時也需要知道如何對硬碟分區，如何使用終端機。你永遠不知道什麼時候你的圖形界面會壞掉，然後你需要使用命令列修復。

頻繁的內核更新有時還會導致驅動的問題，而你需要去手動排錯。

### SSL 憑證問題

Manjaro 開發人員曾多次無法更新他們的 SSL 憑證。第一次是在 2015 年，當時他們推薦使用者去**回滾他們的系統時鐘**作爲因應措施[^3]。第二次是在 2016 年，他們推薦使用者在瀏覽器中新增例外以繞過限制[^4]。

### 部分更新

Manjaro 團隊建議使用者使用[部分更新](https://wiki.archlinux.org/index.php/System_maintenance#Avoid_certain_pacman_commands)[^5]，而這是[不被支援的](https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported)並且有可能導致[系統損壞](https://gist.github.com/vodik/5660494)。

### Yaourt 和 Pamac

Manjaro 釋出時自帶 Pamac[^5]，這是一個 Pacman 的前端，同時也可作爲 [AUR Helper](https://wiki.archlinux.org/index.php/AUR_helpers) 使用。而 AUR Helper 不會教使用者如何去使用 AUR。同時，AUR 並不安全，因此你需要在打包之前檢查 PKGBUILD 指令檔。盲目安裝來自 AUR 的軟體包可能會損害你的系統。

在 Manjaro 使用 Pamac 之前，還曾自帶 Yaourt，這是一個十分老舊且不安全的 AUR Helper。

### 商業化

Manjaro 正在商業化。他們售賣預裝 Manjaro 的電腦[^6]。這樣是好是壞你可以自行斟酌，但是 Arch Linux 僅接受捐款。

### 一些瑣事

Manjaro 的系統更新指令檔在更新過程中會執行 `rm` 指令以刪除 lockfile（譯者註：該檔案是用於避免兩個包管理器實例同時執行時造成衝突的問題）[^7]。該指令檔還會執行 `pacman -Q | grep`，然而 Pacman 本身已經支援了查詢包的功能。

一個可用於本地阻斷服務攻擊和提權的漏洞[8]在他們的 Bash 指令檔中被發現。

他們的 Linux 模組在模組目錄下執行 `rm` 指令[^9]。

Manjaro 使用機器人欺騙 Distrowatch 分數[^2][^10]。

他們經常要求使用者[重新下載](https://gist.github.com/Brottweiler/952c8b0de0afc01c6c8ef18b5a1a5294)整個 Pacman 資料庫，而這個操作應該僅在資料庫損壞時執行[^14]。

### 手動降級 Systemd

2019 年一月，Manjaro 釋出了新的穩定版本。同時，systemd 也更新一個新的主要版本。Manjaro 維護他們自己的 systemd 包，然而他們的包似乎導致了人們的系統無法開機。

Manjaro 團隊建議使用者去開啓降級選項[^11]，用於在更新系統時降級他們的 systemd 以避免損壞。Pacman 支援使用 `epoch` 變數以避免降級，但 Manjaro 沒有使用[^12]。

在註釋連結中的“重要通知”似乎已從主公告中被移除[^13]，現在僅能在另一個帖子中找到。

### 總結

使用 Manjaro，你只能得到更差的支援，更糟糕的軟體包質量和無從談起的安全性[^2]；

> 他們只是轉發了我們的建議而從來不閱讀它們。任由緊急的安全問題在所謂的「穩定」倉庫中腐爛，僅僅修復一些被使用者所公開告知的問題。[^10]

同時 Manjaro 從未對上游作出過貢獻[^2]。

這個 Gist 中所出現的問題在任何 Arch 衍生發行版中都十分普遍，其中 Manjaro 最甚。請考慮使用原版的 Arch Linux，或者選擇一個不基於 Arch Linux 的發行版。

### 所以你該使用什麼？

如果你因爲 Arch Linux 而使用 Manjaro，你應該[安裝 Arch Linux](https://wiki.archlinux.org/index.php/installation_guide)。確保只遵循官方的安裝指南，而不是任何其他的指南、文章或是一個 YouTube 影片。

如果你只是單純想使用一個漸進式升級發行版，或者你不喜歡 Arch Linux，可以考慮 [openSUSE Tumbleweed](https://en.opensuse.org/Portal:Tumbleweed)

如果你需要一個新手友好且穩定的發行版，可以選擇一個長期支援的發行版。可以嘗試任何 [Ubuntu 衍生版](https://www.ubuntu.com/download/flavours) 或 [openSUSE Leap](https://en.opensuse.org/Portal:Leap)

[^1]: <https://manjaro.org> (<https://archive.fo/pBN8X>)

[^2]: <https://reddit.com/comments/adf6cx/_/edgpidc> (<https://archive.fo/TwuVC>)

[^3]: <https://manjaro.github.io/expired_SSL_certificate/> (<https://web.archive.org/web/20150409095421/>)

[^4]: <http://manjaro.github.io:80/SSL-Certificate-Expired/> (<https://web.archive.org/web/20171203081155/>)

[^5]: <https://forum.manjaro.org/t/pamac-introducing-our-own-aur-support/17924>

[^6]: <https://manjaro.org/hardware-bladebook> <https://manjaro.org/hardware-spitfire>

[^7]: <https://gitlab.manjaro.org/packages/core/manjaro-system/blob/3b806753e245b7ec7e18bb674e916e28d751a429/manjaro-update-system.sh#L45> (<https://archive.fo/dofw8>)

[^8]: <https://lists.manjaro.org/pipermail/manjaro-security/2018-August/000785.html> (<https://archive.fo/L6NYn>)

[^9]: <https://forum.manjaro.org/t/usr-lib-modules-getting-deleted-on-boot/49984>

[^10]: <https://reddit.com/comments/9ur2lu/_/e96qch1> (<https://archive.fo/DTZGs>)

[^11]: <https://forum.manjaro.org/t/no-longer-able-to-boot-after-latest-update/73014/3>

[^12]: <https://reddit.com/comments/ajclsq/_/eeuzv75/> (<https://archive.fo/dPfyn>)

[^13]: <https://forum.manjaro.org/t/stable-update-2019-01-23-kernels-mesa-browsers-nvidia-deepin-virtualbox>

[^14]: <https://forum.manjaro.org/t/stable-update-2019-02-19-kernels-kde-libreoffice-systemd-virtualbox-deepin-qt-firmwares-wine/76420/2>

原文：[Manjaro Controversies](https://rentry.co/manjaro-controversies)

參考（zh_CN）：[【译】 Manjaro 悖论](https://github.com/szclsya/blog/blob/master/content/posts/linux/manjaro_controversies.zh-cn.org)
