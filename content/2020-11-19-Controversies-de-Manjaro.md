+++
title = "(Tradusion) Controversies de Manjaro"
date = 2020-11-19

[taxonomies]
tags = ["Arch", "Linux", "Manjaro", "Conlang"]
categories = ["Linux"]
+++

### Introdusion

[Manjaro](https://zh.wikipedia.org/wiki/Manjaro_Linux) esti un distribusion de Linux basid sur [Arch Linux](https://zh.wikipedia.org/wiki/Arch_Linux). Ile concentrii sur accesibiliti, amiabiliti et stabiliti.

Mais, habi habid i controversias maior circ Manjaro. Quet gist esti por adresar tut controversias conosid circ Manjaro.

<!-- more -->

#### Stabiliti

> Manjaro [...] esti facile a utilizar et stable

Manjaro se proclami un distro stable de Linux, mais tut quet que iles facii esti manutenar las paquetes de Arch un setman por "probar", quet que iles confirmi que sas changes no rompi las instalasiones de sas utilizadores.

### Convenable por principandes

> Manjaro esti adequand convenable por principandes et utilizadores experiencides de Linux.
>
> Manjaro esti convenable por principandes. [...][^1]

Manjaro esti un distro de publicid rodand, et las distros de publicid rodand *no* debi estar utilizid par principandes de razones varies.

Tu debi confirmar ta sistem esti sempre a-di, quela involvi un actuelizasion por un setman minim.

Depot de paquetes esti publicid quand esti prest, tu debi estar preparid por las problemes potensionales et las buges.

Quand utilizar un distro de publicid rodand, tu debi estar familiare con las resolusiones de problemes, cognosar quomo la gerador de paquetes funsioni, cognosar quom las paquetes interacti ensimule et quom las dependensis funsioni. Quom partisionar un disc, et quom utilizar la terminale; Tu iamais cognosi quand un GUI rompi et tu debi reparar es via la linie de comand.

Actuelizasiones frequentes de kernele a vice posi cosar las problemes con controladores, et tu debi resolvar las problemes.

### Problemes de certificasion SSL

Divelopadors de Manjaro habi falid repetidomend renovar sas certificasiones SSL. La prim vice in 2015 quo iles recomendid a las utilizadores **reveniar sas horologis de sistem** quom un solusion provisionale[^3]. La secund vice in 2016 iles recomendid a las utilizadores adar un exepsion in sas navigador quom un bypass[^4].

### Actuelizasiones parsiales

La Grup de Manjaro sugeri a utilizadores executar un [actuelizasion parsiale](https://wiki.archlinux.org/index.php/System_maintenance#Avoid_certain_pacman_commands)[^5] quela esti [no suportid](https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported) et posi cosar [rompar ta sistem](https://gist.github.com/vodik/5660494).

### Yaourt et Pamac

Manjaro esti publicid con Pamac[^5], quet esti un interface por pacman et funsioni quom un [asistador AUR](https://wiki.archlinux.org/index.php/AUR_helpers). Asistadores AUR no ensigni las utilizadores [quom utilizar la AUR](https://wiki.archlinux.org/index.php/Arch_User_Repository). La AUR esti insecure et tu debi inspesionar la PKGBUILD ant de construiar. La instalasion de paquetes de AUR çeco posi estar nocivo a ta sistem.

Ant de Pamac, Manjaro habi estid publicid con Yaourt quela esti un asistador antic et insecure.

### Monetizasion

Manjaro se concentri sur monetizasion de la distro. Iles vendi computadores con Manjaro instalid[^6]. Prendi ta proprie decision si tu pensi quet esti bon o male, mais Arch Linux sole accepti donasiones.

### Coses miscelanies dutes

La script de actuelizasion de sistem executi `rm` sur la lockfile mid-transasion[^7]. Quet script tamben executi `pacman -Q | grep` quand pacman ia suporti natibmend la interogasion de paquetes.

Un vulnerabiliti de DoS locale, PrivEsc[^8] esti encontrid in sas script bash.

Sas module Linux executid `rm` sur la directorie de modules[^9].

Manjaro falsifici sas punctuasion de distrowatch con bots[^2][^10].

Ile sugeri sovend a utilizadores [ritelechargar](https://gist.github.com/Brottweiler/952c8b0de0afc01c6c8ef18b5a1a5294) tut la bas de dat de pacman quand quet debi sol estar executid quand habi i un bas de dat de pacman coruptid[^14].

### Digradasion manuale de systemd

In januarie 2019 un nob version Stable de Manjaro esti publicid. Quet esti a la quom temp quand un maior version de systemd esti actuelizid. Manjaro manuteni sas proprie paquet de systemd, et ile simule habi rendido las sistemes de utilizadores incapables de lanzar.

La Grup de Manjaro consuliid utilizadores de activar la opsion de digradasion[^11] quand actuelizar sas sistemes por digradar systemd, por evitar rupture. Pacman suporti la variable epoch por evitar digradasion, mais Manjaro no habi utilizid quet[^12].

La "Avise important" in la citasion conectid simule habi estid suprimid de la publicasion principale[^13] et existi sol in quet citasion de altre file.

### Conclusion

Con Manjaro, tu vadi te terminar con suport, paquetar et securiti plus popre[^2];

> Iles transferi just nas avises de securiti sin ligi quetes. Ile laxi poriar las problemes critices de securiti in sas repositories "stable" et sol porti avant problemes que esti publicid o que las utilizadores se parli.[^10]

Manjaro no contributi a upstream[^2].

Las problemes in quet gist esti comun con tut las derivides de Arch, mais particularmend mal con Manjaro. Consideri utlizar Arch Linux pure, o utilizi un altre distro no esti basid sur Arch Linux.

### Alore, que debi tu utilizar?

Si tu consideri utilizar Manjaro porque tu desideri utilizar [Arch Linux](https://www.archlinux.org), tu debi [instalar Arch Linux](https://wiki.archlinux.org/index.php/installation_guide). Asecuri de sequar sol la guid oficiale de instalasion, et no tut altres guides, articules, o un vidie youtube tu discoberi.

Si tu desideri sol un distro de publicid rodand, o tu no ami Arch Linux, tu posi consultar [openSUSE Tumbleweed](https://en.opensuse.org/Portal:Tumbleweed).

Si tu desideri un distro stable et amiable par principandes, tu debi utilizar un distro Long Term Support. Tu posi consultar tut [las versiones de Ubuntu](https://www.ubuntu.com/download/flavours) o [openSUSE Leap](https://en.opensuse.org/Portal:Leap).

[^1]: <https://manjaro.org> (<https://archive.fo/pBN8X>)

[^2]: <https://reddit.com/comments/adf6cx/_/edgpidc> (<https://archive.fo/TwuVC>)

[^3]: <https://manjaro.github.io/expired_SSL_certificate/> (<https://web.archive.org/web/20150409095421/>)

[^4]: <http://manjaro.github.io:80/SSL-Certificate-Expired/> (<https://web.archive.org/web/20171203081155/>)

[^5]: <https://forum.manjaro.org/t/pamac-introducing-our-own-aur-support/17924>

[^6]: <https://manjaro.org/hardware-bladebook> <https://manjaro.org/hardware-spitfire>

[^7]: <https://gitlab.manjaro.org/packages/core/manjaro-system/blob/3b806753e245b7ec7e18bb674e916e28d751a429/manjaro-update-system.sh#L45> (<https://archive.fo/dofw8>)

[^8]: <https://lists.manjaro.org/pipermail/manjaro-security/2018-August/000785.html> (<https://archive.fo/L6NYn>)

[^9]: <https://forum.manjaro.org/t/usr-lib-modules-getting-deleted-on-boot/49984>

[^10]: <https://reddit.com/comments/9ur2lu/_/e96qch1> (<https://archive.fo/DTZGs>)

[^11]: <https://forum.manjaro.org/t/no-longer-able-to-boot-after-latest-update/73014/3>

[^12]: <https://reddit.com/comments/ajclsq/_/eeuzv75/> (<https://archive.fo/dPfyn>)

[^13]: <https://forum.manjaro.org/t/stable-update-2019-01-23-kernels-mesa-browsers-nvidia-deepin-virtualbox>

[^14]: <https://forum.manjaro.org/t/stable-update-2019-02-19-kernels-kde-libreoffice-systemd-virtualbox-deepin-qt-firmwares-wine/76420/2>

Origine: [Manjaro Controversies](https://rentry.co/manjaro-controversies)
